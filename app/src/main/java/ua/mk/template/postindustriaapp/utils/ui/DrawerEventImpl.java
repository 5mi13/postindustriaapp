package ua.mk.template.postindustriaapp.utils.ui;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import ua.mk.template.postindustriaapp.navigation.MainScreenOpenParams;

public class DrawerEventImpl implements DrawerEvent {

    private PublishSubject<MainScreenOpenParams> subject = PublishSubject.create();

    @Override
    public void selectInNavigationMenu(MainScreenOpenParams mainScreenOpenParams) {
        subject.onNext(mainScreenOpenParams);
    }

    @Override
    public Observable<MainScreenOpenParams> getEventsEmitter() {
        return subject;
    }
}

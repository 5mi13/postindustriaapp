package ua.mk.template.postindustriaapp.presentation.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ua.mk.template.postindustriaapp.R;
import ua.mk.template.postindustriaapp.models.Status;
import ua.mk.template.postindustriaapp.presentation.view.OnTwitterViewEventsListener;
import ua.mk.template.postindustriaapp.utils.ui.delegates.AdapterDelegate;

public class TwitterAdapterDelegate implements AdapterDelegate<Status> {

    private final OnTwitterViewEventsListener onTwitterViewEventsListener;

    public TwitterAdapterDelegate(OnTwitterViewEventsListener onTwitterViewEventsListener) {
        this.onTwitterViewEventsListener = onTwitterViewEventsListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        final View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_twitter, parent,false);
        return new TwitterViewHolder(itemView, onTwitterViewEventsListener);
    }

    @Override
    public void onBindViewHolder(@NonNull Status items, int position, @NonNull RecyclerView.ViewHolder holder) {
        final TwitterViewHolder twitterViewHolder = (TwitterViewHolder) holder;
        twitterViewHolder.mapData(items);
    }
}
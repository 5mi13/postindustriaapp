package ua.mk.template.postindustriaapp.di.modules;

import android.app.Application;
import android.content.Context;

import java.io.File;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ua.mk.template.postindustriaapp.di.scopes.CacheDir;

@Module
public class ApplicationModule {
    private Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return application;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return application;
    }

    @Provides
    @CacheDir
    File provideCacheDir() {
        return application.getCacheDir();
    }
}

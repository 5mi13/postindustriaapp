package ua.mk.template.postindustriaapp.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class Entities {

    @JsonProperty("hashtags")
    public List<Hashtag> hashtags = null;
    @JsonProperty("user_mentions")
    public List<UserMention> userMentions = null;
}

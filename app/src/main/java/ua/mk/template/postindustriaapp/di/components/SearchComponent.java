package ua.mk.template.postindustriaapp.di.components;

import dagger.Subcomponent;
import ua.mk.template.postindustriaapp.di.manager.ComponentManager;
import ua.mk.template.postindustriaapp.presentation.search.SearchFragment;

@Subcomponent
public interface SearchComponent {
    void inject(SearchFragment searchFragment);

    class Manager {
        private static final Class<SearchComponent> COMPONENT_CLASS = SearchComponent.class;

        public static SearchComponent create() {
            ComponentManager componentManager = ComponentManager.getInstance();
            if (componentManager.hasComponent(COMPONENT_CLASS)) {
                throw new IllegalStateException("SearchComponent has already been created");
            }

            return componentManager.getComponent(MainActivityComponent.class)
                    .plusMainComponent();
        }

        public static boolean isPresent() {
            return ComponentManager.getInstance().hasComponent(COMPONENT_CLASS);
        }

        public static void save(SearchComponent mainActivityComponent) {
            ComponentManager.getInstance().addComponent(mainActivityComponent);
        }

        public static SearchComponent get() {
            return ComponentManager.getInstance().getComponent(COMPONENT_CLASS);
        }

        public static void remove() {
            ComponentManager.getInstance().removeComponent(COMPONENT_CLASS);
        }
    }
}

package ua.mk.template.postindustriaapp.navigation;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.util.SparseArray;

import java.util.Objects;

import ua.mk.template.postindustriaapp.R;

public class MainNavigationRouteFabric {
    private static final SparseArray<String> routeMap;

    static {
        routeMap = new SparseArray<>();
        routeMap.put(R.id.nav_search, Screens.SCREEN_SEARCH);
        routeMap.put(R.id.nav_nearby, Screens.SCREEN_NEARBY);
        routeMap.put(R.id.nav_favorites, Screens.SCREEN_FAVORITES);
    }

    @NonNull
    public static String getScreenKey(@IdRes int menuItemId) {
        return Objects.requireNonNull(routeMap.get(menuItemId));
    }

    @IdRes
    public static int getScreenId(String screen) {
        for (int i = 0; i < routeMap.size(); i++) {
            String value = routeMap.valueAt(i);
            if (value.equals(screen)) {
                return routeMap.keyAt(i);
            }
        }
        return 0;
    }
}

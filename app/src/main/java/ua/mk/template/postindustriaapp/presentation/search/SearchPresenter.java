package ua.mk.template.postindustriaapp.presentation.search;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import ua.mk.template.postindustriaapp.presentation.base.BasePresenter;

import static ua.mk.template.postindustriaapp.utils.Utils.applySchedulers;


@InjectViewState
public class SearchPresenter extends BasePresenter<SearchView> {

    private static final String TAG = SearchPresenter.class.getName();
    private static final String DEFAULT_SEARCH_QUERY = "new";
    private String searchQuery;

    @Inject
    SearchPresenter() {
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        loadTweets();
    }

    @SuppressLint("CheckResult")
    public void search(String searchQuery) {
        Disposable disposable = mainInteractor.search(searchQuery)
                .compose(mapAndLocalSaveData())
                .compose(applyFavorites())
                .compose(applySchedulers())
                .subscribe(this::handleResponse,
                        this::handleError);
        unsubscribeOnDestroy(disposable);
    }

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected String getDefaultQuery() {
        return TextUtils.isEmpty(searchQuery) ? DEFAULT_SEARCH_QUERY : searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }
}
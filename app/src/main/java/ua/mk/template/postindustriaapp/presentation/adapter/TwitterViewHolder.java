package ua.mk.template.postindustriaapp.presentation.adapter;

import android.view.View;

import butterknife.BindView;
import ua.mk.template.postindustriaapp.R;
import ua.mk.template.postindustriaapp.models.Status;
import ua.mk.template.postindustriaapp.presentation.view.OnTwitterViewEventsListener;
import ua.mk.template.postindustriaapp.presentation.view.TwitterView;
import ua.mk.template.postindustriaapp.utils.ui.holders.BaseViewHolder;

public class TwitterViewHolder extends BaseViewHolder<Status> {
    @BindView(R.id.twitterItem)
    TwitterView twitterView;
    public TwitterViewHolder(View itemView, OnTwitterViewEventsListener onTwitterViewEventsListener) {
        super(itemView);
        twitterView.setOnTwitterViewEventsListener(onTwitterViewEventsListener);
    }

    @Override
    public void mapData(Status data) {
        twitterView.mapData(data);
    }
}

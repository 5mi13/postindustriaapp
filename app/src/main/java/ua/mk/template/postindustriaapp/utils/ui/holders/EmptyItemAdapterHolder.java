package ua.mk.template.postindustriaapp.utils.ui.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public class EmptyItemAdapterHolder extends RecyclerView.ViewHolder {
    public EmptyItemAdapterHolder(View itemView) {
        super(itemView);
    }
}

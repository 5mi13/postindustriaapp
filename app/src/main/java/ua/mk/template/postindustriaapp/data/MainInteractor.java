package ua.mk.template.postindustriaapp.data;


import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import java.util.Locale;

import io.reactivex.Observable;
import ua.mk.template.postindustriaapp.models.TwitterResponse;

import static ua.mk.template.postindustriaapp.utils.Utils.generateQueryFromString;

public class MainInteractor {

    private static final String TAG = MainInteractor.class.getName();

    public static final int PAGE_SIZE = 20;
    private static final int RANGE = 10;
    private final ApiTwitter apiTwitter;

    public MainInteractor(ApiTwitter apiTwitter) {
        this.apiTwitter = apiTwitter;
    }

    public Observable<TwitterResponse> search(String searchQuery) {
        return apiTwitter.search(searchQuery, PAGE_SIZE);
    }

    public Observable<TwitterResponse> getNextPage(@NonNull String nextQuery) {
        return apiTwitter.getNextPage(generateQueryFromString(nextQuery));
    }

    public Observable<TwitterResponse> refresh(@NonNull String refreshQuery) {
        return apiTwitter.refresh(generateQueryFromString(refreshQuery));
    }

    public Observable<TwitterResponse> nearBy(String searchQuery, double latitude, double longitude) {
        return apiTwitter.nearBy(searchQuery, generateGeoQuery(latitude, longitude), PAGE_SIZE);
    }

    @SuppressLint("DefaultLocale")
    private String generateGeoQuery(double latitude, double longitude) {
        return String.format(Locale.US, "%f,%f,%dkm", latitude, longitude, RANGE);
    }
}
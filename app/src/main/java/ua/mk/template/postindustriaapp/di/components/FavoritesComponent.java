package ua.mk.template.postindustriaapp.di.components;

import dagger.Subcomponent;
import ua.mk.template.postindustriaapp.di.manager.ComponentManager;
import ua.mk.template.postindustriaapp.presentation.favorites.FavoritesFragment;

@Subcomponent
public interface FavoritesComponent {
    void inject(FavoritesFragment mainFragment);

    class Manager {
        private static final Class<FavoritesComponent> COMPONENT_CLASS = FavoritesComponent.class;

        public static FavoritesComponent create() {
            ComponentManager componentManager = ComponentManager.getInstance();
            if (componentManager.hasComponent(COMPONENT_CLASS)) {
                throw new IllegalStateException("FavoritesComponent has already been created");
            }

            return componentManager.getComponent(MainActivityComponent.class)
                    .plusFavoritesComponent();
        }

        public static boolean isPresent() {
            return ComponentManager.getInstance().hasComponent(COMPONENT_CLASS);
        }

        public static void save(FavoritesComponent mainActivityComponent) {
            ComponentManager.getInstance().addComponent(mainActivityComponent);
        }

        public static FavoritesComponent get() {
            return ComponentManager.getInstance().getComponent(COMPONENT_CLASS);
        }

        public static void remove() {
            ComponentManager.getInstance().removeComponent(COMPONENT_CLASS);
        }
    }
}

package ua.mk.template.postindustriaapp.presentation.nearby;

import android.content.Context;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import javax.inject.Inject;

import ua.mk.template.postindustriaapp.R;
import ua.mk.template.postindustriaapp.di.components.HasComponent;
import ua.mk.template.postindustriaapp.di.components.NearbyComponent;
import ua.mk.template.postindustriaapp.presentation.base.BaseFragment;
import ua.mk.template.postindustriaapp.presentation.base.BasePresenter;

public class NearbyFragment extends BaseFragment implements NearbyView, HasComponent<NearbyComponent> {

    private static final String TAG = NearbyFragment.class.getName();
    private NearbyComponent component;

    @Inject
    @InjectPresenter
    NearbyPresenter presenter;

    @ProvidePresenter
    public NearbyPresenter providePresenter() {
        return presenter;
    }

    public static NearbyFragment newInstance() {
        return new NearbyFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        getComponent().inject(this);
    }

    @Override
    public NearbyComponent getComponent() {
        if (component == null) {
            component = NearbyComponent.Manager.create();
        }
        return component;
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected String getFragmentTag() {
        return TAG;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_nearby;
    }
}
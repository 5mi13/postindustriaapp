package ua.mk.template.postindustriaapp.presentation.favorites;

import com.arellomobile.mvp.MvpView;

import java.util.List;

import ua.mk.template.postindustriaapp.models.Status;

interface FavoritesView extends MvpView {

    void showData(List<Status> statuses);
}

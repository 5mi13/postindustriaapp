package ua.mk.template.postindustriaapp.navigation;

public class Screens {
    public static final String SCREEN_SEARCH = "SEARCH";

    public static final String SCREEN_NEARBY = "NEARBY";

    public static final String SCREEN_FAVORITES = "FAVORITES";

}

package ua.mk.template.postindustriaapp.presentation.base;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.util.Log;

import com.arellomobile.mvp.MvpPresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import ua.mk.template.postindustriaapp.data.FavoriteInteractor;
import ua.mk.template.postindustriaapp.data.MainInteractor;
import ua.mk.template.postindustriaapp.models.Status;
import ua.mk.template.postindustriaapp.models.TwitterResponse;

import static ua.mk.template.postindustriaapp.utils.Utils.applyCompletableSchedulers;
import static ua.mk.template.postindustriaapp.utils.Utils.applySchedulers;

public abstract class BasePresenter<T extends BaseView> extends MvpPresenter<T> {

    @Inject
    protected MainInteractor mainInteractor;
    @Inject
    protected FavoriteInteractor favoriteInteractor;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private TwitterResponse twitterResponse;

    protected void loadTweets() {
        if (twitterResponse == null) {
            search(getDefaultQuery());
        } else {
            handleResponse(twitterResponse.statuses);
        }
    }

    @SuppressLint("CheckResult")
    public void loadNextPage() {
        if (twitterResponse.searchMetaData.nextResults != null) {
            Disposable disposable = mainInteractor.getNextPage(twitterResponse.searchMetaData.nextResults)
                    .compose(mapAndLocalSaveData())
                    .compose(applyFavorites())
                    .compose(applySchedulers())
                    .subscribe(this::handleNextResponse,
                            this::handleError);
            unsubscribeOnDestroy(disposable);
        }
    }

    @SuppressLint("CheckResult")
    public void refresh() {
        if (twitterResponse.searchMetaData.nextResults != null) {
            Disposable disposable = mainInteractor.refresh(twitterResponse.searchMetaData.nextResults)
                    .compose(mapAndLocalSaveData())
                    .compose(applyFavorites())
                    .compose(applySchedulers())
                    .subscribe(this::handleRefreshResponse,
                            this::handleError);
            unsubscribeOnDestroy(disposable);
        } else {
            getViewState().refreshIsCompleted();
        }
    }

    @NonNull
    protected ObservableTransformer<TwitterResponse, List<Status>> mapAndLocalSaveData() {
        return observable -> observable
                .doOnNext(this::setTwitterResponse)
                .map(twitterResponse -> twitterResponse.statuses);
    }

    protected ObservableTransformer<List<Status>, List<Status>> applyFavorites() {
        return source -> source.flatMap(statusList ->
                Observable.fromCallable(() -> favoriteInteractor.readOrCreateStatusesFile())
                        .compose(applySchedulers())
                        .map(favorites ->
                                updateResponseByFavorites(favorites, statusList)));
    }

    private List<Status> updateResponseByFavorites(List<Status> favorites, List<Status> statusList) {
        if (!favorites.isEmpty()) {
            for (Status status : statusList) {
                for (Status favorite : favorites) {
                    if (status.equals(favorite)) {
                        status.isFavorite = favorite.isFavorite;
                    }
                }
            }
        }
        return statusList;
    }

    private void setTwitterResponse(TwitterResponse twitterResponse) {
        this.twitterResponse = twitterResponse;
    }

    protected void handleResponse(List<Status> statuses) {
        getViewState().showData(statuses);
    }

    private void handleNextResponse(List<Status> statuses) {
        getViewState().addNextData(statuses);
    }

    private void handleRefreshResponse(List<Status> statuses) {
        getViewState().refreshData(statuses);
    }

    protected void handleError(Throwable throwable) {
        Log.d(getTag(), "Could not get data: " + throwable.getMessage());
    }

    @SuppressLint("CheckResult")
    public void saveToFavorites(Status status) {
        Completable.fromAction(() -> favoriteInteractor.saveOrRemoveFavorite(status))
                .compose(applyCompletableSchedulers())
                .subscribe();
    }

    protected void unsubscribeOnDestroy(@NonNull Disposable subscription) {
        compositeDisposable.add(subscription);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }

    protected abstract String getTag();

    protected abstract void search(String searchQuery);

    protected abstract String getDefaultQuery();
}
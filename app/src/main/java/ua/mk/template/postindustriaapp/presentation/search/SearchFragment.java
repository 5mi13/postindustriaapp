package ua.mk.template.postindustriaapp.presentation.search;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.eccyan.optional.Optional;
import com.hendraanggrian.widget.SocialEditText;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import ua.mk.template.postindustriaapp.R;
import ua.mk.template.postindustriaapp.di.components.HasComponent;
import ua.mk.template.postindustriaapp.di.components.SearchComponent;
import ua.mk.template.postindustriaapp.models.fragment.NavigationInfo;
import ua.mk.template.postindustriaapp.presentation.base.BaseFragment;
import ua.mk.template.postindustriaapp.presentation.base.BasePresenter;


public class SearchFragment extends BaseFragment implements SearchView, HasComponent<SearchComponent> {

    private static final String TAG = SearchFragment.class.getName();
    private static final String ARGUMENT_SEARCH_QUERY = "ARGUMENT_SEARCH_QUERY";
    private SearchComponent component;

    @Inject
    @InjectPresenter
    SearchPresenter presenter;

    @ProvidePresenter
    public SearchPresenter providePresenter() {
        return presenter;
    }

    @BindView(R.id.inputSearch)
    SocialEditText inputSearch;

    public static SearchFragment newInstance(NavigationInfo navigationInfo) {
        Bundle args = new Bundle();
        args.putParcelable(ARGUMENT_SEARCH_QUERY, navigationInfo);
        SearchFragment fragment = new SearchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        getComponent().inject(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSearchQueryArgument().ifPresent(presenter::setSearchQuery);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupInput();
    }

    @SuppressLint("CheckResult")
    private void setupInput() {
        RxTextView.textChanges(inputSearch)
                .debounce(100, TimeUnit.MILLISECONDS)
                .map(CharSequence::toString)
                .filter(chars -> !TextUtils.isEmpty(chars))
                .subscribe(presenter::search);
    }

    @Override
    public SearchComponent getComponent() {
        if (component == null) {
            component = SearchComponent.Manager.create();
        }
        return component;
    }

    public Optional<String> getSearchQueryArgument() {
        return getNavigationInfoArgument().flatMap(navigationInfo ->
                Optional.ofNullable(navigationInfo.getSearchQuery()));
    }

    private Optional<NavigationInfo> getNavigationInfoArgument() {
        return Optional.ofNullable(getArguments())
                .flatMap(bundle ->
                        Optional.ofNullable(bundle.getParcelable(ARGUMENT_SEARCH_QUERY)));
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected String getFragmentTag() {
        return TAG;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_search;
    }
}
package ua.mk.template.postindustriaapp.presentation.nearby;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import ua.mk.template.postindustriaapp.data.LocationInteractor;
import ua.mk.template.postindustriaapp.presentation.base.BasePresenter;

import static ua.mk.template.postindustriaapp.utils.Utils.applySchedulers;


@InjectViewState
public class NearbyPresenter extends BasePresenter<NearbyView> {

    private final static String DEFAULT_SEARCH_QUERY = "";
    private static final String TAG = NearbyPresenter.class.getName();

    @Inject
    LocationInteractor locationInteractor;

    @Inject
    NearbyPresenter() {
    }

    @SuppressLint("CheckResult")
    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        loadTweets();
    }

    @SuppressLint("CheckResult")
    public void search(String searchQuery) {
        locationInteractor.lastKnownLocation()
                .compose(applySchedulers())
                .flatMap(location ->
                        mainInteractor.nearBy(searchQuery, location.getLatitude(), location.getLongitude())
                                .compose(mapAndLocalSaveData())
                                .compose(applyFavorites())
                                .compose(applySchedulers()))
                .subscribe(this::handleResponse,
                        this::handleError);
    }

    @Override
    protected String getDefaultQuery() {
        return DEFAULT_SEARCH_QUERY;
    }

    @Override
    protected String getTag() {
        return TAG;
    }
}
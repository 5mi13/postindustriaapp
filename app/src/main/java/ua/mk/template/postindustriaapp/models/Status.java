package ua.mk.template.postindustriaapp.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigInteger;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class Status {

    @JsonProperty("created_at")
    public String createdAt;
    @JsonProperty("id")
    public BigInteger id;
    @JsonProperty("id_str")
    public String idStr;
    @JsonProperty("text")
    public String text;
    @JsonProperty("entities")
    public Entities entities;
    @JsonProperty("extended_entities")
    public ExtendedEntities extendedEntities;
    @JsonProperty("user")
    public User user;
    @JsonProperty("favorite")
    public boolean isFavorite;

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Status && this.idStr.equals(((Status) obj).idStr);
    }
}
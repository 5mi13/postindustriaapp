package ua.mk.template.postindustriaapp.presentation.base;

import com.arellomobile.mvp.MvpView;

import java.util.List;

import ua.mk.template.postindustriaapp.models.Status;


public interface BaseView extends MvpView {
    void showData(List<Status> statuses);

    void addNextData(List<Status> statuses);

    void refreshData(List<Status> statuses);

    void  refreshIsCompleted();
}

package ua.mk.template.postindustriaapp.presentation.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import ua.mk.template.postindustriaapp.models.Status;
import ua.mk.template.postindustriaapp.presentation.view.OnTwitterViewEventsListener;
import ua.mk.template.postindustriaapp.utils.ui.delegates.AbstractDelegateManager;
import ua.mk.template.postindustriaapp.utils.ui.delegates.AdapterDelegate;
import ua.mk.template.postindustriaapp.utils.ui.delegates.EmptyAdapterDelegate;

public class TwitterDelegateManager implements AbstractDelegateManager<Status> {
    private static final int ITEM_TWITTER = 10;
    private final AdapterDelegate twitterAdapterDelegate;

    public TwitterDelegateManager(OnTwitterViewEventsListener onTwitterViewEventsListener) {
        twitterAdapterDelegate = new TwitterAdapterDelegate(onTwitterViewEventsListener);
    }

    @Override
    public int getItemViewType(@NonNull Status items, int position) {
        return ITEM_TWITTER;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return getAdapterDelegate(viewType).onCreateViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull Status items, int position, @NonNull RecyclerView.ViewHolder viewHolder) {
        int itemViewType = viewHolder.getItemViewType();
        AdapterDelegate delegate = getAdapterDelegate(itemViewType);
        if (delegate != null) {
            delegate.onBindViewHolder(items, position, viewHolder);
        }
    }

    private AdapterDelegate getAdapterDelegate(int itemViewType) {
        switch (itemViewType) {
            case ITEM_TWITTER:
                return twitterAdapterDelegate;
            default:
                return new EmptyAdapterDelegate();
        }
    }
}
package ua.mk.template.postindustriaapp.di.components;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Component;
import ua.mk.template.postindustriaapp.di.modules.ApplicationModule;
import ua.mk.template.postindustriaapp.di.modules.BaseConfigurationModule;
import ua.mk.template.postindustriaapp.di.modules.LocationModule;
import ua.mk.template.postindustriaapp.di.modules.NavigationModule;
import ua.mk.template.postindustriaapp.di.modules.TwitterApiModule;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        BaseConfigurationModule.class,
        NavigationModule.class,
        TwitterApiModule.class,
})
public interface ApplicationComponent {

    void inject(Application application);

    MainActivityComponent plusMainActivityComponent(LocationModule locationModule);
}
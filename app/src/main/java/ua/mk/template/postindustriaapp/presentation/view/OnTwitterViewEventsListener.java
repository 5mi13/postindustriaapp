package ua.mk.template.postindustriaapp.presentation.view;

import ua.mk.template.postindustriaapp.models.Status;

public interface OnTwitterViewEventsListener {

    void onLinkedTextClicked(String hashtag);

    void onFavoriteClicked(Status status);
}

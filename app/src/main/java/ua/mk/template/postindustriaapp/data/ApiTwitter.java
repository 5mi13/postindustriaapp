package ua.mk.template.postindustriaapp.data;


import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import ua.mk.template.postindustriaapp.models.TwitterResponse;

public interface ApiTwitter {

    @GET("search/tweets.json")
    Observable<TwitterResponse> search(
            @Query("q") String query,
            @Query("count") int pageSize);

    @GET("search/tweets.json")
    Observable<TwitterResponse> getNextPage(@QueryMap Map<String, String> nextQuery);

    @GET("search/tweets.json")
    Observable<TwitterResponse> refresh(@QueryMap Map<String, String> refreshQuery);

    @GET("search/tweets.json")
    Observable<TwitterResponse> nearBy(@Query("q") String query,
                                       @Query(value = "geocode", encoded = true) String geoRequest,
                                       @Query("count") int pageSize);
}

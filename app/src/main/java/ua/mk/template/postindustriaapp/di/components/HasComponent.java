package ua.mk.template.postindustriaapp.di.components;

public interface HasComponent<COMPONENT> {
    COMPONENT getComponent();
}

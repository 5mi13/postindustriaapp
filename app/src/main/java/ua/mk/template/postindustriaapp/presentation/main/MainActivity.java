package ua.mk.template.postindustriaapp.presentation.main;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ua.mk.template.postindustriaapp.R;
import ua.mk.template.postindustriaapp.R2;
import ua.mk.template.postindustriaapp.di.components.MainActivityComponent;
import ua.mk.template.postindustriaapp.navigation.MainDrawerNavigator;
import ua.mk.template.postindustriaapp.navigation.MainScreenOpenParams;
import ua.mk.template.postindustriaapp.utils.ui.DrawerEvent;

public class MainActivity extends MvpAppCompatActivity implements MainActivityView {

    private static final String TAG = AppCompatActivity.class.getName();

    @Inject
    NavigatorHolder navigatorHolder;
    @Inject
    DrawerEvent drawerEvent;
    @Inject
    @InjectPresenter
    MainActivityPresenter presenter;

    @ProvidePresenter
    public MainActivityPresenter providePresenter() {
        return presenter;
    }

    @BindView(R2.id.toolbar)
    Toolbar toolbar;
    @BindView(R2.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R2.id.nav_view)
    NavigationView navigationView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        getComponent().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupToolbarAndDrawer();
        processIntent(getIntent());
    }

    private void processIntent(Intent intent) {
        MainScreenOpenParams openParams = MainScreenOpenParams.fromBundle(intent.getExtras());
        presenter.navigateTo(openParams);
    }

    private void setupToolbarAndDrawer() {
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this::onMenuItemSelected);
        drawerMenuListener();
    }

    @SuppressLint("CheckResult")
    private void drawerMenuListener() {
        drawerEvent.getEventsEmitter()
                .subscribe(presenter::navigateTo);
    }

    public void setupNavigator(boolean shouldEnable) {
        if (navigatorHolder == null) {
            Log.d(TAG, "Cannot %s navigator while holder is null! " + (shouldEnable ? "enable" : "disable"));
            return;
        }
        if (!shouldEnable) {
            navigatorHolder.removeNavigator();
            return;
        }
        Navigator navigator = provideNavigator();
        if (navigator != null) {
            navigatorHolder.setNavigator(navigator);
        }
    }

    private Navigator provideNavigator() {
        return new MainDrawerNavigator(this, R.id.fragment_container);
    }

    private boolean onMenuItemSelected(MenuItem item) {
        presenter.onNavigationItemClicked(item.getItemId());
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void selectInNavigationMenu(int itemId) {
        Log.d(TAG, "Clicked ");
        navigationView.setCheckedItem(itemId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupNavigator(true);
    }

    @Override
    protected void onPause() {
        setupNavigator(false);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        presenter.onBackPressed();
    }

    private MainActivityComponent getComponent() {
        return MainActivityComponent.Manager.isPresent()
                ? MainActivityComponent.Manager.get()
                : MainActivityComponent.Manager.createAndSave(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isFinishing()) {
            MainActivityComponent.Manager.remove();
        }
    }
}
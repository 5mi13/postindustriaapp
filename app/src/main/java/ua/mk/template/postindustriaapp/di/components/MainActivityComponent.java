package ua.mk.template.postindustriaapp.di.components;

import android.support.v7.app.AppCompatActivity;

import dagger.Subcomponent;
import ua.mk.template.postindustriaapp.di.manager.ComponentManager;
import ua.mk.template.postindustriaapp.di.modules.ApiInteractorModule;
import ua.mk.template.postindustriaapp.di.modules.DrawerEventModule;
import ua.mk.template.postindustriaapp.di.modules.FavoriteModule;
import ua.mk.template.postindustriaapp.di.modules.LocationModule;
import ua.mk.template.postindustriaapp.di.scopes.MainActivityScope;
import ua.mk.template.postindustriaapp.presentation.main.MainActivity;

@MainActivityScope
@Subcomponent(modules = {ApiInteractorModule.class,
        FavoriteModule.class,
        LocationModule.class,
        DrawerEventModule.class})
public interface MainActivityComponent {
    void inject(MainActivity activity);

    SearchComponent plusMainComponent();

    FavoritesComponent plusFavoritesComponent();

    NearbyComponent plusNearbyComponent();

    class Manager {
        private static final Class<MainActivityComponent> COMPONENT_CLASS = MainActivityComponent.class;

        public static MainActivityComponent createAndSave(AppCompatActivity activity) {
            ComponentManager componentManager = ComponentManager.getInstance();
            if (componentManager.hasComponent(COMPONENT_CLASS)) {
                throw new IllegalStateException("MainActivityComponent has already been created");
            }
            LocationModule locationModule = new LocationModule(activity);
            MainActivityComponent mainActivityComponent = componentManager.getApplicationComponent()
                    .plusMainActivityComponent(locationModule);
            return componentManager.addComponent(mainActivityComponent);
        }

        public static boolean isPresent() {
            return ComponentManager.getInstance().hasComponent(COMPONENT_CLASS);
        }

        public static void save(MainActivityComponent mainActivityComponent) {
            ComponentManager.getInstance().addComponent(mainActivityComponent);
        }

        public static MainActivityComponent get() {
            return ComponentManager.getInstance().getComponent(COMPONENT_CLASS);
        }

        public static void remove() {
            ComponentManager.getInstance().removeComponent(COMPONENT_CLASS);
        }
    }
}

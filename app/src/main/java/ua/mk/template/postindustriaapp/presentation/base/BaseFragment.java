package ua.mk.template.postindustriaapp.presentation.base;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.jakewharton.rxbinding2.support.v7.widget.RxRecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import ua.mk.template.postindustriaapp.R;
import ua.mk.template.postindustriaapp.data.MainInteractor;
import ua.mk.template.postindustriaapp.models.Status;
import ua.mk.template.postindustriaapp.presentation.adapter.TwitterAdapter;
import ua.mk.template.postindustriaapp.presentation.view.OnTwitterViewEventsListener;

import static android.graphics.Color.BLUE;
import static android.graphics.Color.CYAN;
import static android.graphics.Color.GREEN;
import static android.graphics.Color.RED;


public abstract class BaseFragment extends MvpAppCompatFragment implements BaseView, OnTwitterViewEventsListener, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.twitterList)
    RecyclerView recyclerView;
    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private TwitterAdapter twitterAdapter;
    private boolean loadingStatus;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResource(), container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupRecyclerView();
        setupSwipeRefreshLayout();
    }

    @Override
    public void showData(List<Status> statuses) {
        twitterAdapter.setData(statuses);
    }

    @Override
    public void addNextData(List<Status> statuses) {
        twitterAdapter.addNextData(statuses);
    }

    @Override
    public void refreshData(List<Status> statuses) {
        twitterAdapter.addRefreshedData(statuses);
        recyclerView.smoothScrollToPosition(0);
        refreshIsCompleted();
    }

    private void setupSwipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(RED, GREEN, BLUE, CYAN);
    }

    private void setupRecyclerView() {
        twitterAdapter = new TwitterAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(twitterAdapter);
        setupEndlessScroll();
    }

    @SuppressLint("CheckResult")
    private void setupEndlessScroll() {
        RxRecyclerView.scrollEvents(recyclerView)
                .filter(scrollInfo -> scrollInfo.dy() > 0)
                .map(result -> ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition())
                .distinctUntilChanged()
                .filter(lastVisibleItemPosition -> lastVisibleItemPosition >=
                        (twitterAdapter.getItemCount() - (MainInteractor.PAGE_SIZE / 2)))
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(lastVisibleItemPosition -> {
                            getPresenter().loadNextPage();
                            Log.d(getFragmentTag(),
                                    "load next page, findLastVisibleItemPosition : "
                                            + lastVisibleItemPosition);
                        },
                        throwable -> Log.e(getTag(),
                                "Could not listen for scroll events in timeline",
                                throwable));
    }

    @Override
    public void onLinkedTextClicked(String linkedQuery) {
        getPresenter().search(linkedQuery);
    }

    @Override
    public void onFavoriteClicked(Status status) {
        getPresenter().saveToFavorites(status);
    }

    @Override
    public void onRefresh() {
        if (!loadingStatus) {
            getPresenter().refresh();
        } else {
            refreshIsCompleted();
        }
    }

    public void refreshIsCompleted() {
        swipeRefreshLayout.setRefreshing(false);
    }

    protected abstract BasePresenter getPresenter();

    protected abstract String getFragmentTag();

    @LayoutRes
    protected abstract int getLayoutResource();
}
package ua.mk.template.postindustriaapp.navigation;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.commands.Back;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Replace;
import ru.terrakok.cicerone.commands.SystemMessage;
import ua.mk.template.postindustriaapp.models.fragment.NavigationInfo;
import ua.mk.template.postindustriaapp.presentation.favorites.FavoritesFragment;
import ua.mk.template.postindustriaapp.presentation.search.SearchFragment;
import ua.mk.template.postindustriaapp.presentation.nearby.NearbyFragment;

import static ua.mk.template.postindustriaapp.navigation.Screens.SCREEN_FAVORITES;
import static ua.mk.template.postindustriaapp.navigation.Screens.SCREEN_SEARCH;
import static ua.mk.template.postindustriaapp.navigation.Screens.SCREEN_NEARBY;

public class MainDrawerNavigator implements Navigator {
    private final FragmentActivity activity;
    private final FragmentManager fragmentManager;
    private final int containerId;

    public MainDrawerNavigator(FragmentActivity activity, int containerId) {
        this.activity = activity;
        this.containerId = containerId;
        this.fragmentManager = activity.getSupportFragmentManager();
    }

    @Override
    public void applyCommands(Command[] commands) {
        for (Command command : commands) applyCommand(command);
    }

    private void applyCommand(Command command) {
        if (command instanceof Back) {
            activity.finish();
        } else if (command instanceof SystemMessage) {
            Toast.makeText(activity, ((SystemMessage) command).getMessage(), Toast.LENGTH_SHORT).show();
        } else if (command instanceof Replace) {

            Replace replace = (Replace) command;
            String screenKey = replace.getScreenKey();
            Object transitionData = replace.getTransitionData();

            Fragment fragment = createFragment(screenKey, transitionData);
            if (fragment != null) {
                setFragment(fragment);
                return;
            }
        }
    }

    private Fragment createFragment(String screenKey, Object transitionData) {
        switch (screenKey) {
            case SCREEN_SEARCH:
                return SearchFragment.newInstance((NavigationInfo) transitionData);
            case SCREEN_NEARBY:
                return NearbyFragment.newInstance();
            case SCREEN_FAVORITES:
                return FavoritesFragment.newInstance();
            default:
                throw new RuntimeException("Unknown screen key!");
        }
    }

    private void setFragment(Fragment fragment) {
        fragmentManager.beginTransaction()
                .replace(containerId, fragment)
                .commit();
    }
}

package ua.mk.template.postindustriaapp.configuration;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.util.Arrays;
import java.util.List;

import dagger.Module;
import okhttp3.Interceptor;

@Module
public class BaseConfiguration implements Configuration {
    private final static String AUTHORIZATION_TOKEN = "AAAAAAAAAAAAAAAAAAAAAACi6AAAAAAAuFPuBpve%2B6S8QWqrS3t7t9uZV7k%3DwUbAifBNR5aeu3Yv2T88WIEtBtVDtr2lHbSMZqDAaaUJWmle9c";
    private final static String BASE_URL = "https://api.twitter.com/";
    private final static String API_VERSION = "1.1";
    private final static String URL_SEPARATOR = "/";

    @Override
    public String getTwitterApiUrl() {
        return BASE_URL + API_VERSION + URL_SEPARATOR;
    }

    @Override
    public List<Interceptor> getAppInterceptors() {
        Interceptor[] interceptors = {
                new ApiInterceptor(AUTHORIZATION_TOKEN),
                new StethoInterceptor()
        };
        return Arrays.asList(interceptors);
    }
}

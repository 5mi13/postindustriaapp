package ua.mk.template.postindustriaapp.utils.ui;

import io.reactivex.Observable;
import ua.mk.template.postindustriaapp.navigation.MainScreenOpenParams;

public interface DrawerEvent {

    void selectInNavigationMenu(MainScreenOpenParams mainScreenOpenParams);

    Observable<MainScreenOpenParams> getEventsEmitter();
}

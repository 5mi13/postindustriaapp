package ua.mk.template.postindustriaapp.models.fragment;

import android.os.Parcel;
import android.os.Parcelable;

public class NavigationInfo implements Parcelable {

    private String searchQuery;

    public NavigationInfo(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    private NavigationInfo(Parcel in) {
        searchQuery = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeString(searchQuery);
    }

    public static final Creator<NavigationInfo> CREATOR = new Creator<NavigationInfo>() {
        @Override
        public NavigationInfo createFromParcel(Parcel in) {
            return new NavigationInfo(in);
        }

        @Override
        public NavigationInfo[] newArray(int size) {
            return new NavigationInfo[size];
        }
    };

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }
}

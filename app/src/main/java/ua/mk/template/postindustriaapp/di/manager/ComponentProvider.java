package ua.mk.template.postindustriaapp.di.manager;

import android.support.annotation.NonNull;

public interface ComponentProvider {
    @NonNull
    <T> T getComponent(Class<T> clazz);
}

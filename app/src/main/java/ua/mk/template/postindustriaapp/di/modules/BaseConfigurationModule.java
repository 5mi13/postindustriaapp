package ua.mk.template.postindustriaapp.di.modules;

import dagger.Module;
import dagger.Provides;
import ua.mk.template.postindustriaapp.configuration.BaseConfiguration;
import ua.mk.template.postindustriaapp.configuration.Configuration;

@Module
public class BaseConfigurationModule {

    @Provides
    Configuration provideConfiguration() {
        return new BaseConfiguration();
    }
}

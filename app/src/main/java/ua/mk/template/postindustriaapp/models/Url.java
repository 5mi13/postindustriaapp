package ua.mk.template.postindustriaapp.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class Url {

    @JsonProperty("url")
    public String url;
    @JsonProperty("expanded_url")
    public String expandedUrl;
    @JsonProperty("display_url")
    public String displayUrl;
    @JsonProperty("indices")
    public List<Integer> indices = null;
}

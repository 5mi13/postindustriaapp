package ua.mk.template.postindustriaapp.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Nullable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchMetaData {

    @Nullable
    @JsonProperty("next_results")
    public String nextResults;
    @JsonProperty("query")
    public String query;
    @Nullable
    @JsonProperty("refresh_url")
    public String refreshUrl;

}

package ua.mk.template.postindustriaapp.presentation.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.hendraanggrian.widget.SocialTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import ua.mk.template.postindustriaapp.R;
import ua.mk.template.postindustriaapp.di.modules.GlideApp;
import ua.mk.template.postindustriaapp.models.Status;

public class TwitterView extends FrameLayout {

    @BindView(R.id.authorAvatar)
    ImageView authorAvatar;
    @BindView(R.id.authorName)
    TextView authorName;
    @BindView(R.id.twitText)
    SocialTextView twitText;
    @BindView(R.id.isFavorite)
    CheckBox isFavorite;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private OnTwitterViewEventsListener onTwitterViewEventsListener;
    private Status status;

    public TwitterView(Context context) {
        super(context);
        init();
    }

    public TwitterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TwitterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.partial_twitter_view, this);
        ButterKnife.bind(this);
        setupSocialTextView();
    }

    private void setupSocialTextView() {
        twitText.setOnHashtagClickListener((textView, hashtag) -> {
            onTwitterViewEventsListener.onLinkedTextClicked("#" + hashtag);
            return null;
        });
        twitText.setOnMentionClickListener((textView, mention) -> {
            onTwitterViewEventsListener.onLinkedTextClicked("@" + mention);
            return null;
        });
    }

    public void mapData(Status status) {
        this.status = status;
        authorName.setText(status.user.name);
        twitText.setText(status.text);
        isFavorite.setChecked(status.isFavorite);
        progressBar.setVisibility(VISIBLE);
        GlideApp.with(this)
                .load(status.user.profileAvatar)
                .transform(new CircleCrop())
                .listener(avatarLoadingListener())
                .into(authorAvatar);
    }

    @NonNull
    private RequestListener<Drawable> avatarLoadingListener() {
        return new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                progressBar.setVisibility(GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                progressBar.setVisibility(GONE);
                return false;
            }
        };
    }

    public void setOnTwitterViewEventsListener(OnTwitterViewEventsListener onTwitterViewEventsListener) {
        this.onTwitterViewEventsListener = onTwitterViewEventsListener;
    }

    @OnCheckedChanged(R.id.isFavorite)
    public void onFavoriteChecked(CompoundButton compoundButton, boolean checked) {
        status.isFavorite = checked;
        onTwitterViewEventsListener.onFavoriteClicked(status);
    }
}
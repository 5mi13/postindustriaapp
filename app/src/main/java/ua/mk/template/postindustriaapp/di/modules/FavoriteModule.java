package ua.mk.template.postindustriaapp.di.modules;

import java.io.File;

import dagger.Module;
import dagger.Provides;
import ua.mk.template.postindustriaapp.data.FavoriteInteractor;
import ua.mk.template.postindustriaapp.di.scopes.CacheDir;

@Module
public class FavoriteModule {

    @Provides
    FavoriteInteractor provideFavoriteInteractor(@CacheDir File caheDir){
        return new FavoriteInteractor(caheDir);
    }
}
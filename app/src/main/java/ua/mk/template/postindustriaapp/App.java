package ua.mk.template.postindustriaapp;

import android.app.Application;

import com.facebook.stetho.Stetho;

import ua.mk.template.postindustriaapp.di.components.ApplicationComponent;
import ua.mk.template.postindustriaapp.di.manager.ComponentManager;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        inject();
    }

    private void inject() {
        ComponentManager.init(this);
        ApplicationComponent appComponent = ComponentManager.getInstance()
                .getApplicationComponent();
        appComponent.inject(this);
    }
}
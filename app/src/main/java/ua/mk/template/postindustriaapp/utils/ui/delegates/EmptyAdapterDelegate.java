package ua.mk.template.postindustriaapp.utils.ui.delegates;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ua.mk.template.postindustriaapp.R;
import ua.mk.template.postindustriaapp.utils.ui.holders.EmptyItemAdapterHolder;

public class EmptyAdapterDelegate implements AdapterDelegate {

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_empty, parent, false);
        return new EmptyItemAdapterHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Object items, int position, @NonNull RecyclerView.ViewHolder holder) {
    }
}
package ua.mk.template.postindustriaapp.presentation.favorites;

import android.annotation.SuppressLint;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;
import ua.mk.template.postindustriaapp.data.FavoriteInteractor;
import ua.mk.template.postindustriaapp.models.Status;
import ua.mk.template.postindustriaapp.models.fragment.NavigationInfo;
import ua.mk.template.postindustriaapp.navigation.MainScreenOpenParams;
import ua.mk.template.postindustriaapp.utils.ui.DrawerEvent;

import static ua.mk.template.postindustriaapp.navigation.Screens.SCREEN_SEARCH;
import static ua.mk.template.postindustriaapp.utils.Utils.applyCompletableSchedulers;
import static ua.mk.template.postindustriaapp.utils.Utils.applySchedulers;


@InjectViewState
public class FavoritesPresenter extends MvpPresenter<FavoritesView> {

    private static final String TAG = FavoritesPresenter.class.getName();

    @Inject
    FavoriteInteractor favoriteInteractor;
    @Inject
    DrawerEvent drawerEvent;
    @Inject
    FavoritesPresenter() {
    }

    @SuppressLint("CheckResult")
    public void getFavorites() {
        Observable.fromCallable(() -> favoriteInteractor.readOrCreateStatusesFile())
                .compose(applySchedulers())
                .subscribe(this::handleResponse,
                        this::handleError);
    }

    private void handleError(Throwable throwable) {
        Log.d(TAG, "Could not get data: " + throwable.getMessage());
    }

    private void handleResponse(List<Status> statuses) {
        getViewState().showData(statuses);
    }

    @SuppressLint("CheckResult")
    public void saveToFavorites(Status status) {
        Completable.fromAction(() -> favoriteInteractor.saveOrRemoveFavorite(status))
                .compose(applyCompletableSchedulers())
                .subscribe();
    }

    public void openMainFragment(String linkedQuery) {
        NavigationInfo navigationInfo = new NavigationInfo(linkedQuery);
        MainScreenOpenParams mainScreenOpenParams = new MainScreenOpenParams(SCREEN_SEARCH, navigationInfo);
        drawerEvent.selectInNavigationMenu(mainScreenOpenParams);
    }
}
package ua.mk.template.postindustriaapp.configuration;

import java.util.List;

import okhttp3.Interceptor;

public interface Configuration {
    String getTwitterApiUrl();

    List<Interceptor> getAppInterceptors();
}
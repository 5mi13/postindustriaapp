package ua.mk.template.postindustriaapp.di.modules;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;
import ua.mk.template.postindustriaapp.configuration.Configuration;
import ua.mk.template.postindustriaapp.data.ApiTwitter;

@Module
public class TwitterApiModule {

    @Provides
    OkHttpClient provideOkHttpClient(Configuration baseConfiguration) {

        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();

        for (Interceptor interceptor : baseConfiguration.getAppInterceptors()) {
            okHttpBuilder.addNetworkInterceptor(interceptor);
        }
        return okHttpBuilder.build();
    }

    @Provides
    Retrofit provideRetrofit(Configuration baseConfiguration, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(baseConfiguration.getTwitterApiUrl())
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
    }

    @Provides
    ApiTwitter provideApiTwitter(Retrofit retrofit){
        return retrofit.create(ApiTwitter.class);
    }
}

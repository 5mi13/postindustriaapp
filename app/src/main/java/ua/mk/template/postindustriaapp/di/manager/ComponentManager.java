package ua.mk.template.postindustriaapp.di.manager;

import android.app.Application;
import android.support.annotation.NonNull;

import ua.mk.template.postindustriaapp.di.components.ApplicationComponent;
import ua.mk.template.postindustriaapp.di.components.DaggerApplicationComponent;
import ua.mk.template.postindustriaapp.di.modules.ApplicationModule;

public class ComponentManager implements ComponentProvider {
    private static volatile ComponentManager INSTANCE;
    private final ComponentMap componentMap = new ComponentMap();
    private final ApplicationComponent appComponent;

    private ComponentManager(ApplicationComponent appComponent) {
        this.appComponent = appComponent;
    }

    //region Initialization

    public static synchronized void init(Application application) {
        if (INSTANCE != null) {
            throw new IllegalStateException("ComponentManager has already been initialized");
        }
        ApplicationComponent appComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(application))
                .build();

        @SuppressWarnings("UnnecessaryLocalVariable") //need for
                ComponentManager componentManager = new ComponentManager(appComponent);
        INSTANCE = componentManager;
    }

    public static ComponentManager getInstance() {
        if (INSTANCE == null) {
            throw new IllegalStateException("ComponentManager hasn't been initialized");
        }
        return INSTANCE;
    }

    //endregion

    public ApplicationComponent getApplicationComponent() {
        return appComponent;
    }

    public <T> T addComponent(T component) {
        componentMap.addComponent(component);
        return component;
    }

    public boolean hasComponent(Class<?> clazz) {
        return componentMap.hasComponent(clazz);
    }

    public void removeComponent(Class<?> clazz) {
        componentMap.remove(clazz);
    }

    @NonNull
    @Override
    public <T> T getComponent(Class<T> clazz) {
        if (clazz == ApplicationComponent.class) {
            return clazz.cast(getApplicationComponent());
        }

        return componentMap.getComponent(clazz);
    }
}

package ua.mk.template.postindustriaapp.data;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ua.mk.template.postindustriaapp.di.scopes.CacheDir;
import ua.mk.template.postindustriaapp.models.Status;

public class FavoriteInteractor {

    private final File cacheDir;

    private static final String TAG = FavoriteInteractor.class.getName();

    public FavoriteInteractor(@CacheDir File cacheDir) {
        this.cacheDir = cacheDir;
    }

    public void saveOrRemoveFavorite(Status status) {
        ObjectMapper mapper = new ObjectMapper();
        List<Status> statuses;
        statuses = readOrCreateStatusesFile();

        statuses = findAndRemoveExistingStatus(statuses, status);

        try {
            mapper.writeValue(getFile(), statuses);
        } catch (IOException e) {
            Log.e(TAG, "Could not save data to file: " + e.getMessage());
        }
    }

    public List<Status> readOrCreateStatusesFile() {
        ObjectMapper mapper = new ObjectMapper();
        File favorites = getFile();
        if (favorites.exists()) {
            try {
                return mapper.readValue(favorites, mapper.getTypeFactory().constructCollectionType(List.class, Status.class));
            } catch (IOException e) {
                Log.e(TAG, "Could not read data from file: " + e.getMessage());
                return new ArrayList<>();
            }
        } else {
            return new ArrayList<>();
        }
    }

    private List<Status> findAndRemoveExistingStatus(List<Status> statuses, Status status) {
        if (statuses.contains(status) && !status.isFavorite) {
            statuses.remove(status);
        } else if (!statuses.contains(status) && status.isFavorite) {
            statuses.add(0, status);
        }
        return statuses;
    }

    public File getFile() {
        return new File(cacheDir, "favorites.json");
    }
}

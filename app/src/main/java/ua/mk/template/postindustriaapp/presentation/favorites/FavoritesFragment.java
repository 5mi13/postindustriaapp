package ua.mk.template.postindustriaapp.presentation.favorites;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.mk.template.postindustriaapp.R;
import ua.mk.template.postindustriaapp.di.components.FavoritesComponent;
import ua.mk.template.postindustriaapp.di.components.HasComponent;
import ua.mk.template.postindustriaapp.models.Status;
import ua.mk.template.postindustriaapp.presentation.adapter.TwitterAdapter;
import ua.mk.template.postindustriaapp.presentation.view.OnTwitterViewEventsListener;


public class FavoritesFragment extends MvpAppCompatFragment implements FavoritesView, OnTwitterViewEventsListener, HasComponent<FavoritesComponent> {

    private FavoritesComponent component;

    @Inject
    @InjectPresenter
    FavoritesPresenter presenter;

    @ProvidePresenter
    public FavoritesPresenter providePresenter() {
        return presenter;
    }

    @BindView(R.id.twitterList)
    RecyclerView recyclerView;
    private TwitterAdapter twitterAdapter;

    public static FavoritesFragment newInstance() {
        return new FavoritesFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        getComponent().inject(this);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorites, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupRecyclerView();
        presenter.getFavorites();
    }

    private void setupRecyclerView() {
        twitterAdapter = new TwitterAdapter(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(twitterAdapter);
    }

    @Override
    public void onLinkedTextClicked(String linkedQuery) {
        presenter.openMainFragment(linkedQuery);
    }

    @Override
    public void onFavoriteClicked(Status status) {
        presenter.saveToFavorites(status);
    }

    @Override
    public FavoritesComponent getComponent() {
        if (component == null) {
            component = FavoritesComponent.Manager.create();
        }
        return component;
    }

    @Override
    public void showData(List<Status> statuses) {
        twitterAdapter.setData(statuses);
    }
}
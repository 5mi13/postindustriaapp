package ua.mk.template.postindustriaapp.di.modules;

import android.support.v7.app.AppCompatActivity;

import com.patloew.rxlocation.RxLocation;
import com.tbruyelle.rxpermissions2.RxPermissions;

import dagger.Module;
import dagger.Provides;
import ua.mk.template.postindustriaapp.di.scopes.MainActivityScope;

@Module
public class LocationModule {
    private final AppCompatActivity context;

    public LocationModule(AppCompatActivity context) {
        this.context = context;
    }

    @Provides
    @MainActivityScope
    RxLocation provideRxLocation() {
        return new RxLocation(context);
    }

    @Provides
    @MainActivityScope
    RxPermissions provideRxPermissions() {
        return new RxPermissions(context);
    }
}

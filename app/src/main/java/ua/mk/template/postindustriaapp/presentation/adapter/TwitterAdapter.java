package ua.mk.template.postindustriaapp.presentation.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ua.mk.template.postindustriaapp.models.Status;
import ua.mk.template.postindustriaapp.presentation.view.OnTwitterViewEventsListener;
import ua.mk.template.postindustriaapp.utils.ui.delegates.AbstractDelegateManager;

public class TwitterAdapter extends RecyclerView.Adapter {

    private List<Status> statuses = new ArrayList<>();
    AbstractDelegateManager delegateManager;


    public TwitterAdapter(OnTwitterViewEventsListener onTwitterViewEventsListener) {
        delegateManager = new TwitterDelegateManager(onTwitterViewEventsListener);
    }

    public void setData(List<Status> data) {
        statuses.clear();
        statuses.addAll(data);
        notifyDataSetChanged();
    }

    public void addNextData(List<Status> data) {
        int currentPosition = getItemCount() + 1;
        int countAddedData = 0;
        for (Status status : data) {
            if (!statuses.contains(status)) {
                statuses.add(status);
                countAddedData++;
            }
        }
        notifyItemRangeInserted(currentPosition, countAddedData);
    }

    public void addRefreshedData(List<Status> data) {
        int countAddedData = 0;
        for (int i = data.size() - 1; i >= 0; i--) {
            Status status = data.get(i);
            if (!statuses.contains(status)) {
                statuses.add(0, status);
                countAddedData++;
            }
        }
        notifyItemRangeInserted(0, countAddedData);
    }

    @Override
    public int getItemViewType(int position) {
        return delegateManager.getItemViewType(statuses.get(position), position);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return delegateManager.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        delegateManager.onBindViewHolder(statuses.get(position), position, holder);
    }

    @Override
    public int getItemCount() {
        return statuses.size();
    }
}
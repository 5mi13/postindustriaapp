package ua.mk.template.postindustriaapp.di.modules;

import dagger.Module;
import dagger.Provides;
import ua.mk.template.postindustriaapp.di.scopes.MainActivityScope;
import ua.mk.template.postindustriaapp.utils.ui.DrawerEvent;
import ua.mk.template.postindustriaapp.utils.ui.DrawerEventImpl;

@Module
public class DrawerEventModule {

    @Provides
    @MainActivityScope
    DrawerEvent provideDrawerEvent(){
        return new DrawerEventImpl();
    }
}

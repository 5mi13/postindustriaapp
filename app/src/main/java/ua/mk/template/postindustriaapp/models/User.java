package ua.mk.template.postindustriaapp.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigInteger;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class User {

    @JsonProperty("id")
    public BigInteger id;
    @JsonProperty("id_str")
    public String idStr;
    @JsonProperty("name")
    public String name;
    @JsonProperty("screen_name")
    public String screenName;
    @JsonProperty("description")
    public String description;
    @JsonProperty("url")
    public String url;
    @JsonProperty("entities")
    public UserEntities entities;
    @JsonProperty("profile_image_url")
    public String profileAvatar;
}

package ua.mk.template.postindustriaapp.presentation.main;

import com.arellomobile.mvp.MvpView;

public interface MainActivityView extends MvpView {
    void selectInNavigationMenu(int itemId);
}

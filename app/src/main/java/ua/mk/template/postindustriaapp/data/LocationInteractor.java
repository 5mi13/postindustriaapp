package ua.mk.template.postindustriaapp.data;

import android.annotation.SuppressLint;
import android.location.Location;
import android.util.Log;

import com.patloew.rxlocation.RxLocation;
import com.tbruyelle.rxpermissions2.RxPermissions;

import io.reactivex.Completable;
import io.reactivex.Observable;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class LocationInteractor {
    private static final String TAG = LocationInteractor.class.getName();
    private final RxLocation rxLocation;
    private final RxPermissions rxPermissions;

    public LocationInteractor(RxLocation rxLocation, RxPermissions rxPermissions) {
        this.rxLocation = rxLocation;
        this.rxPermissions = rxPermissions;
    }

    @SuppressLint("MissingPermission")
    public Observable<Location> lastKnownLocation() {
        return requestLocationPermission()
                .andThen(rxLocation.location().lastLocation())
                .toObservable();
    }

    private Completable requestLocationPermission() {
        return rxPermissions.request(ACCESS_FINE_LOCATION)
                .flatMapCompletable(permissionGranted -> {
                    if (permissionGranted) {
                        return Completable.complete();
                    } else {
                        return Completable.error(new Exception());
                    }
                })
                .doOnError(throwable -> Log.e(TAG, "Fine location permission was not granted: " + throwable.getMessage()));
    }
}

package ua.mk.template.postindustriaapp.di.modules;

import dagger.Module;
import dagger.Provides;
import ua.mk.template.postindustriaapp.data.ApiTwitter;
import ua.mk.template.postindustriaapp.data.MainInteractor;

@Module
public class ApiInteractorModule {

    @Provides
    MainInteractor provideApiInteractor(ApiTwitter apiTwitter) {
        return new MainInteractor(apiTwitter);
    }
}

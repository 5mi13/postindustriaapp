package ua.mk.template.postindustriaapp.di.components;

import dagger.Subcomponent;
import ua.mk.template.postindustriaapp.di.manager.ComponentManager;
import ua.mk.template.postindustriaapp.di.modules.LocationInteractorModule;
import ua.mk.template.postindustriaapp.presentation.nearby.NearbyFragment;

@Subcomponent(modules = LocationInteractorModule.class)
public interface NearbyComponent {
    void inject(NearbyFragment mainFragment);

    class Manager {
        private static final Class<NearbyComponent> COMPONENT_CLASS = NearbyComponent.class;

        public static NearbyComponent create() {
            ComponentManager componentManager = ComponentManager.getInstance();
            if (componentManager.hasComponent(COMPONENT_CLASS)) {
                throw new IllegalStateException("NearbyComponent has already been created");
            }

            return componentManager.getComponent(MainActivityComponent.class)
                    .plusNearbyComponent();
        }

        public static boolean isPresent() {
            return ComponentManager.getInstance().hasComponent(COMPONENT_CLASS);
        }

        public static void save(NearbyComponent mainActivityComponent) {
            ComponentManager.getInstance().addComponent(mainActivityComponent);
        }

        public static NearbyComponent get() {
            return ComponentManager.getInstance().getComponent(COMPONENT_CLASS);
        }

        public static void remove() {
            ComponentManager.getInstance().removeComponent(COMPONENT_CLASS);
        }
    }
}

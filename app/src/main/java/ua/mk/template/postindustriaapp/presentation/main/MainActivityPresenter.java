package ua.mk.template.postindustriaapp.presentation.main;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;
import ua.mk.template.postindustriaapp.navigation.MainNavigationRouteFabric;
import ua.mk.template.postindustriaapp.navigation.MainScreenOpenParams;

import static ua.mk.template.postindustriaapp.navigation.Screens.SCREEN_SEARCH;

@InjectViewState
public class MainActivityPresenter extends MvpPresenter<MainActivityView> {
    private final Router router;
    private String currentScreen;

    @Inject
    MainActivityPresenter(Router router) {
        this.router = router;
    }

    public void onNavigationItemClicked(int itemId) {
        String screenKey = MainNavigationRouteFabric.getScreenKey(itemId);
        navigateTo(screenKey);
    }

    private void navigateTo(String screenKey){
        navigateTo(new MainScreenOpenParams(screenKey));
    }

    public void navigateTo(MainScreenOpenParams openParams) {
        if (openParams == null) {
            openParams = new MainScreenOpenParams(SCREEN_SEARCH);
        }
        String screenKey = openParams.getSubscreenKey();
        if (!screenKey.equals(currentScreen)) {
            currentScreen = screenKey;
            router.replaceScreen(screenKey, openParams.getScreenParam());
            getViewState().selectInNavigationMenu(MainNavigationRouteFabric.getScreenId(currentScreen));
        }
    }

    public void onBackPressed() {
        router.exit();
    }
}
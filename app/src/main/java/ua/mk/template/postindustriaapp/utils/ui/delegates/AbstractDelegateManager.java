package ua.mk.template.postindustriaapp.utils.ui.delegates;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

public interface AbstractDelegateManager<T> {

    int getItemViewType(@NonNull T items, int position);

    RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType);

    void onBindViewHolder(@NonNull T items, int position, @NonNull RecyclerView.ViewHolder viewHolder);

}
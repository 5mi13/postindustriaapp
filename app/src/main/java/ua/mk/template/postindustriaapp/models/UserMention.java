package ua.mk.template.postindustriaapp.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigInteger;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown=true)
public class UserMention {

    @JsonProperty("screen_name")
    public String screenName;
    @JsonProperty("name")
    public String name;
    @JsonProperty("id")
    public BigInteger id;
    @JsonProperty("id_str")
    public String idStr;
    @JsonProperty("indices")
    public List<BigInteger> indices = null;

}
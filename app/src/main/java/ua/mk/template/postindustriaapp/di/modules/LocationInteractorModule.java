package ua.mk.template.postindustriaapp.di.modules;

import com.patloew.rxlocation.RxLocation;
import com.tbruyelle.rxpermissions2.RxPermissions;

import dagger.Module;
import dagger.Provides;
import ua.mk.template.postindustriaapp.data.LocationInteractor;

@Module
public class LocationInteractorModule {

    @Provides
    LocationInteractor provideLocationInteractor(RxPermissions rxPermissions, RxLocation rxLocation){
        return new LocationInteractor(rxLocation, rxPermissions);
    }
}
